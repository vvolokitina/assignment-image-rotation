//
// Created by Вероника Волокитина on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image image_create(const uint64_t a_width, const uint64_t a_height);

void rotate(struct image *img);

void image_delete(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
