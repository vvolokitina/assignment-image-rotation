//
// Created by Вероника Волокитина on 27.12.2021.
//
#include "image.h"

static size_t get_pixel_offset(struct image const an_image, size_t const a_width_index, size_t const a_height_index);
static struct pixel image_get_pixel(struct image const an_image, size_t const a_width_index, size_t const a_height_index);
static void image_set_pixel(struct image const an_image, struct pixel const a_pixel, size_t const a_width_index, size_t const a_height_index);

static uint64_t calc_width_index(const uint64_t height, const uint64_t index) {
    return height - index - 1;
}

void rotate(struct image *img) {
    const uint64_t trg_height = img->width;
    const uint64_t trg_width = img->height;

    struct image trg_img = image_create(trg_width, trg_height);

    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            const uint64_t mapping_pixel_width_index = calc_width_index(img->height, i);
            const uint64_t mapping_pixel_height_index = j;

            const struct pixel src_pixel = image_get_pixel(img[0], j, i);
            image_set_pixel(trg_img, src_pixel, mapping_pixel_width_index, mapping_pixel_height_index);
        }
    }
    free(img->data);
    *img = trg_img;
}


static struct pixel image_get_pixel(struct image const an_image, size_t const a_width_index, size_t const a_height_index) {
    const size_t pixel_address = get_pixel_offset(an_image, a_width_index, a_height_index);
    return an_image.data[pixel_address];
}

static size_t get_pixel_offset(struct image const an_image, size_t const a_width_index, size_t const a_height_index) {
    return (a_height_index * an_image.width + a_width_index);
}

static void image_set_pixel(struct image const an_image, struct pixel const a_pixel, size_t const a_width_index,
                     size_t const a_height_index) {
    const size_t pixel_address = get_pixel_offset(an_image, a_width_index, a_height_index);
    an_image.data[pixel_address] = a_pixel;
}

