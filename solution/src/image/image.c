//
// Created by Вероника Волокитина on 27.12.2021.
//
#include "image.h"

struct image image_create(uint64_t const a_width, uint64_t const a_height) {
    return (struct image) {.width = a_width, .height = a_height, .data = malloc(
            a_width * a_height * sizeof(struct pixel))};
}

void image_delete(struct image img) {
    free(img.data);
}
