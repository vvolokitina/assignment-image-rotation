//
// Created by Вероника Волокитина on 14.01.2022.
//
#include "status.h"
#include <stdio.h>

void print_status(const enum status return_status) {
    if (status_desc[return_status] == OK) {
        fprintf(stdout, "%s\n", status_desc[return_status]);
    } else {
        fprintf(stderr, "%s\n", status_desc[return_status]);
    }
}
