//
// Created by Вероника Волокитина on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_STATUS_H
#define ASSIGNMENT_IMAGE_ROTATION_STATUS_H

enum status {
    OK,
    OPEN_TO_WRITE_ERROR,
    OPEN_TO_READ_ERROR,
    BMP_PARSE_ERROR,
    CONVERT_ERROR,
    CLOSE_ERROR
};

static const char *const status_desc[] = {
        [OK] = "INFO: OK",
        [OPEN_TO_WRITE_ERROR] = "ERROR: Unable to open bmp file to write",
        [OPEN_TO_READ_ERROR] = "ERROR: Unable to open bmp file to read",
        [BMP_PARSE_ERROR] = "ERROR: Unable to parse bmp file",
        [CONVERT_ERROR] = "ERROR: Unable to convert to bmp file",
        [CLOSE_ERROR] = "ERROR: Unable to close bmp file",
};

void print_status(const enum status return_status);

#endif //ASSIGNMENT_IMAGE_ROTATION_STATUS_H
