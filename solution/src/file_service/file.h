//
// Created by Вероника Волокитина on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include "../utils/status.h"
#include <stdio.h>

struct maybe_file {
    enum status status;
    FILE *file;
};

struct maybe_file file_read_open(const char *file_name);

struct maybe_file file_write_open(const char *file_name);

enum status file_close(FILE *file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
