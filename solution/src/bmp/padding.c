//
// Created by Вероника Волокитина on 27.12.2021.
//

#include "padding.h"

uint32_t calculate_padding(const uint64_t img_width) {
    return 4 * ((img_width * 3 / 4) + 1) - 3 * img_width;
}
