//
// Created by Вероника Волокитина on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_MAPPER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_MAPPER_H

#include "../image/image.h"
#include "../utils/status.h"
#include <stdio.h>

enum status from_bmp(FILE *in, struct image *img);

enum status to_bmp(FILE *out, struct image const *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_MAPPER_H
