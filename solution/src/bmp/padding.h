//
// Created by Вероника Волокитина on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PADDING_H
#define ASSIGNMENT_IMAGE_ROTATION_PADDING_H

#include "../image/image.h"

uint32_t calculate_padding(const uint64_t img_width);

#endif //ASSIGNMENT_IMAGE_ROTATION_PADDING_H
