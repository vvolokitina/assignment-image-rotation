//
// Created by Вероника Волокитина on 27.12.2021.
//

#include "bmp_header.h"
#include "bmp_mapper.h"
#include "padding.h"
#include <stdbool.h>
#include <stdio.h>

static const uint32_t SIGNATURE = 0x4D42;
static const uint32_t HEADER_SIZE = 40;
static const uint16_t PLANES = 1;
static const size_t BIT_COUNT = 24;

struct maybe_header {
    enum status status;
    struct bmp_header bmp_header;
};

static bool read_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

static enum status create_bmp_header(struct image const *img, struct bmp_header *header) {
    uint32_t img_size = (sizeof(struct pixel) * img->width + calculate_padding(img->width)) * img->height;

    header->bfType = SIGNATURE;
    header->bfileSize = sizeof(struct bmp_header) + img_size;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = 0;
    header->biSizeImage = img_size;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    return OK;
}

enum status from_bmp(FILE *in, struct image *img) {

    struct maybe_header maybe_bmp_header;
    maybe_bmp_header.status = (read_header(in, &maybe_bmp_header.bmp_header)) ? OK : BMP_PARSE_ERROR;

    if (maybe_bmp_header.status == BMP_PARSE_ERROR) return BMP_PARSE_ERROR;

    const size_t src_width = maybe_bmp_header.bmp_header.biWidth;
    const size_t src_height = maybe_bmp_header.bmp_header.biHeight;

    *img = image_create(src_width, src_height);

    const uint8_t padding_size = calculate_padding(src_width);
    for (uint64_t i = 0; i < src_height; i++) {
        void *start_index = img->data + img->width * i;

        if (fread(start_index, sizeof(struct pixel), src_width, in) != src_width) return BMP_PARSE_ERROR;
        if (fseek(in, padding_size, SEEK_CUR) != 0) return BMP_PARSE_ERROR;
    }
    return OK;
}

enum status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = {0};
    create_bmp_header(img, &header);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return CONVERT_ERROR;
    if (fseek(out, header.bOffBits, SEEK_SET) != 0) return CONVERT_ERROR;

    const size_t padding = calculate_padding(img->width);

    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; ++i) {
            if (fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out) != 1) {
                return CONVERT_ERROR;
            }
            if (fwrite(img->data, 1, padding, out) != padding) {
                return CONVERT_ERROR;
            }
        }
    }
    return OK;
}
