#include "bmp/bmp_mapper.h"
#include "file_service/file.h"
#include "image/image.h"

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "not enough args\n");
        return 1;
    }

    const char *const SOURCE_IMAGE_NAME = argv[1];
    const char *const TRANSFORM_IMAGE_NAME = argv[2];
    struct image img = {0};
    struct maybe_file input_file = file_read_open(SOURCE_IMAGE_NAME);
    enum status return_status;

    if (input_file.status == OK) {
        if (from_bmp(input_file.file, &img) == OK) {
            rotate(&img);
            struct maybe_file output_file = file_write_open(TRANSFORM_IMAGE_NAME);
            if (output_file.status == OK) {
                return_status = to_bmp(output_file.file, &img) == CONVERT_ERROR;
                file_close(output_file.file);
            } else return_status = OPEN_TO_WRITE_ERROR;
        } else return_status = BMP_PARSE_ERROR;
    } else return_status = OPEN_TO_READ_ERROR;

    image_delete(img);
    print_status(return_status);
    return (return_status == OK) ? 0 : 1;
}
